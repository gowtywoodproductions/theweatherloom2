//
//  Snowflake.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//
import Foundation
import SwiftUI

struct Snowflake: Hashable, Codable, Identifiable {
    var id: Int
    var name: String
    var isFound: Bool

private var imageName: String
 var image: Image {
    Image(imageName)
    }
}
