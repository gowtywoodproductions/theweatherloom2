//
//  ModelData.swift
//  The WeatherLoom
//
//  Created by Owner on 2/11/21.
//

import Foundation
import SwiftUI

final class ModelData: ObservableObject {
    // @Published var landmarks: [Landmark] = load("landmarkData.json")
    //@Published var snowflakes: [Snowflake] = load("snowflake.json")
    
    static var maxSnowflakes = 20
    static var addSnowflakeRate: TimeInterval = 0.2
    
    var lastAddDate: Date = Date(timeIntervalSinceReferenceDate: 0)
   
    var snowflakes = [Snowflake]()
    
    
    init(snowflakeCount: Int) {
        for idx in 0..<snowflakeCount {
            snowflakes.append(Snowflake(id: idx, origin: Self.randomOrigin, z: CGFloat.random(in: 0...1), name: ""))
        }
    }
    
    static var randomOrigin: UnitPoint {
        UnitPoint(x: .random(in: -0.25...1.25), y: .random(in: 0...0.25))
    }
    
    func addSnowflake() {
        snowflakes.append(Snowflake(id: snowflakes.count, name: ""))
    }
    
}

struct Snowflake: Identifiable {
    
   static var eachSnowflake = ""

    let id: Int
    var origin: UnitPoint = ModelData.randomOrigin
    var z: CGFloat = CGFloat.random(in: 0...1)
    var offset: CGFloat = 0
    var name: String
    var image = [String]()
    var removed = 0
    var phase = 0
    
    var lastUpdate: TimeInterval = Date().timeIntervalSinceReferenceDate
    
    static var randomSnowflake: String {
        let idx = Int.random(in: 0..<eachSnowflake.count)
        let index = eachSnowflake.index(eachSnowflake.startIndex, offsetBy: idx)
        return String(eachSnowflake[index])
    }
    
    var stepSize: CGFloat { max(0, 1 / CGFloat(image.count)) }
    
    mutating func addCharacter(_ count: Int = 1) {
        for _ in 0..<count {
            image.append(Self.randomSnowflake)
        }
    }
    
    
    
    mutating func removeFromTop() {
        if removed < image.count {
            image[removed] = ""
            removed += 1
        }
    }
    
    mutating func fallDown(date: Date) {
        offset += 2 + (1 - z) * 5
        
        let ref = date.timeIntervalSinceReferenceDate
        
        if ref > lastUpdate + 0.1 {
            lastUpdate = ref
        } else {
            return
        }
        
        if phase < 11 {
            self.addCharacter()
        } else {
            self.removeFromTop()
        }
        
        phase += 1
        
        if removed >= image.count { reset() }
    }
    
    mutating func reset() {
        offset = 0
        phase = 0
        removed = 0
        image = []
        origin = ModelData.randomOrigin
        z = CGFloat.random(in: 0...1)
    }
}

/*func load<T: Decodable>(_ filename: String) -> T {
    let data: Data

    guard let file = Bundle.main.url(forResource: filename, withExtension: nil)
        else {
            fatalError("Couldn't find \(filename) in main bundle.")
    }

    do {
        data = try Data(contentsOf: file)
    } catch {
        fatalError("Couldn't load \(filename) from main bundle:\n\(error)")
    }

    do {
        let decoder = JSONDecoder()
        return try decoder.decode(T.self, from: data)
    } catch {
        fatalError("Couldn't parse \(filename) as \(T.self):\n\(error)")
    }
}*/
