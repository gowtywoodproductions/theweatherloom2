//
//  The_WeatherLoomApp.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//

import SwiftUI

@main
struct The_WeatherLoomApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
