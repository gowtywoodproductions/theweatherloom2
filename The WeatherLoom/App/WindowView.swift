//
//  WindowView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//

import SwiftUI

struct WindowView: View {
    // MARK: - PROPERTIES
    
    
    // MARK: - BODY
    var body: some View {
        Image("window")
            .resizable()
            .scaledToFill()
            .ignoresSafeArea()
    }
}
// MARK: - PREVIEW
struct WindowView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            WindowView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            // Fallback on earlier versions
        }
    }
}
