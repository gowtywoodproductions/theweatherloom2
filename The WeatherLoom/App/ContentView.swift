//
//  ContentView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//

import SwiftUI

struct ContentView: View {
    // MARK: - PROPERTIES
    @EnvironmentObject var newModelData: NewModelData
        @State var blend:Bool = false
        

    // MARK: - BODY
    var body: some View {
        ZStack {
            HStack(spacing: 0) {
                //Spacer()
                
                // BlueSkyView()
                
                SnowingView()
                // Spacer()
            } //: HSTACK
            WindowView()
            //.ignoresSafeArea()
            
            //Spacer()
            LadyWRearView()
                .position(x: 725, y: 600)
            //WindowView()
            // .scaledToFill()
            
        } //: ZSTACK
    }
}

// MARK: - PREVIEW
struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            ContentView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            // Fallback on earlier versions
        }
        
    }
}
