//
//  WindowFrameView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/11/21.
//

import SwiftUI

struct WindowFrameView: View {
    var body: some View {
        Image("justwindowframe")
            .resizable()
            .scaledToFit()
            
    }
}

struct WindowFrameView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            WindowFrameView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            // Fallback on earlier versions
        }
    }
}
