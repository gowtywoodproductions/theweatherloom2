//
//  DigitalSnow.swift
//  The WeatherLoom
//
//  Created by Owner on 2/11/21.
//

import SwiftUI

struct DigitalSnow: View {
    var body: some View {
        if #available(iOS 15.0, *) {
            TimelineView(.periodic(from: .now, by: 0.1)) { timeline in
                DigitalSnowCanvas(date: timeline.date)
            }
        } else {
            // Fallback on earlier versions
        }
    }
}

struct DigitalSnow_Previews: PreviewProvider {
    static var previews: some View {
        DigitalSnow()
    }
}
