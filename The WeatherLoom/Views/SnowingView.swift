//
//  SnowingView.swift
//  The WeatherLoom
//
//  Created by Owner on 3/11/21.
//

import SwiftUI
import UIKit

struct SnowingView: View {
    // MARK: - PROPERTY
    @State var wish = false
    @State var finishWish = false
    
    // MARK: - BODY
    var body: some View {
        ZStack {
        
       /* VStack {
           
            Button(action: {}, label: {
                Text("Wish")
                    .kerning(2)
                    .font(.title3)
                    .padding(.vertical,12)
                    .padding(.horizontal,50)
                    .background(Color.purple)
                    .clipShape(Capsule())
                    .foregroundColor(.white)
            })
               // .disabled(wish)
        } //: VSTACK */
            
            EmitterView()
                .scaleEffect(!wish ? 1 : 0, anchor: .top)
                .opacity(!wish && !finishWish ? 1 : 0)
            //Moving from Center Effect..
                //.offset(y: wish ? 0 : getRect().height / 2)
                .ignoresSafeArea()
            
        } //: ZSTACK
        .background(Color.black)
        .ignoresSafeArea()
        
    }
    
    /* func doAnimation(){
        withAnimation(.spring()){
            wish = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            
            withAnimation(.easeInOut(duration: 1.5)) {
                finishWish = true
            }
        //Resetting After Wish Finished..
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.4) {
                
                finishWish = false
                wish = false
                
            }
        }
    } */
}


// Emit Particle View
//AKA CAEmitterLayer from UIKIT
struct EmitterView: UIViewRepresentable {
    
    func makeUIView(context: Context) -> UIView {
        
        let view = UIView()
        view.backgroundColor = .clear
        
        // Emitter Layer..
        let emitterLayer = CAEmitterLayer()
        // Since we need top down animation..
        emitterLayer.emitterShape = .line
        emitterLayer.emitterCells = createEmitterCells()
        
        // Size and Position..
        emitterLayer.emitterSize = CGSize(width: getRect().width, height: 1)
        emitterLayer.emitterPosition = CGPoint(x: getRect().width / 2, y: 0)
        
        view.layer.addSublayer(emitterLayer)
        
        
        return view
        
        
    }
    func updateUIView(_ uiView: UIView, context: Context) {
        
    }
    
    func createEmitterCells()->[CAEmitterCell] {
        
       // Multiple Different shaped emitters..
        var emitterCells: [CAEmitterCell] = []
        
        for index in 0...3 {
            
            let cell = CAEmitterCell()
            // Import White Particle Images..
            // Otherwise color won't work
            cell.contents = UIImage(named: getImage(index: index))?.cgImage
           // cell.color = getColor().cgColor
            // New Particle Creation..
            cell.birthRate = 3
            // Particle Existence..
            cell.lifetime = 30
            // Velocity..
            cell.velocity = 30
            // Scale..
            cell.scale = 0.02
            cell.scaleRange = 0.10
            cell.emissionLongitude = .pi
            cell.emissionRange = 0.3
            cell.spin = 0.01
            cell.spinRange = 0.5
            // Acceleration..
            cell.yAcceleration = 0
            
            emitterCells.append(cell)
            
                    }
        
        return emitterCells
    }
    
  
    
    func getImage(index: Int)->String{
        
        if index < 1{
            return "icysnowflake"
        }
        else if index > 1 && index <= 2{
            return "flowersnowflake"
        }
        else if index > 2 && index <= 3{
            return "jewelsnowflake"
        }
        else{
            return "pinksugarsnowflake"
        }
        
    }
}

// MARK: - PREVIEW
struct SnowflakeAnimationView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            SnowingView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            // Fallback on earlier versions
        }
    }
}
