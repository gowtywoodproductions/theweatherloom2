//
//  LadyWRearView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/11/21.
//

import SwiftUI

struct LadyWRearView: View {
    var body: some View {
        Image("ladyw-rearview")
            .resizable()
            .scaledToFit()
            .frame(width: 300, height: 400, alignment: .center)
    }
}

struct LadyWRearView_Previews: PreviewProvider {
    static var previews: some View {
        if #available(iOS 15.0, *) {
            LadyWRearView()
                .previewInterfaceOrientation(.landscapeLeft)
        } else {
            // Fallback on earlier versions
        }
    }
}
