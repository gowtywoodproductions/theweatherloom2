//
//  BlueSkyView.swift
//  The WeatherLoom
//
//  Created by Owner on 30/10/21.
//

import SwiftUI

struct BlueSkyView: View {
    var body: some View {
        Image("Blue sky")
            .resizable()
            .scaledToFill()
            .ignoresSafeArea()
        
    }
}

struct BlueSkyView_Previews: PreviewProvider {
    static var previews: some View {
        BlueSkyView()
        
    }
}
