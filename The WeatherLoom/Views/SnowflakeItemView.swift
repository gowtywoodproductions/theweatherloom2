//
//  SnowflakeView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//

import SwiftUI

struct SnowflakeItemView: View {
    // MARK: - PROPERTIES
    var snowflake: Snowflake
    
    
    // MARK: - BODY
    var body: some View {
        snowflake.image
            .renderingMode(.original)
            .resizable()
            .scaledToFit()
            .frame(width: 30, height: 30, alignment: .center)
    }
}


// MARK: - PREVIEW

struct SnowflakeView_Previews: PreviewProvider {
    static var previews: some View {
        SnowflakeItemView(snowflake: NewModelData().snowflakes[0])
    }
}
