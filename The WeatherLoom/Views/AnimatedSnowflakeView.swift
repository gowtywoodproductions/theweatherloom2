//
//  AnimatedSnowflakeView.swift
//  The WeatherLoom
//
//  Created by Owner on 4/10/21.
//

/*import SwiftUI

struct AnimatedSnowflakeView: View {
    // MARK: - PROPERTIES
    let snowflake = Snowflake.self

    @State var offset: CGSize = .zero
    
    @State private var randomSnowflakes = Int.random(in: 12...16)
    @State private var isAnimating: Bool = false
    @State var positionY = -150
    
    
    // MARK: - FUNCTIONS
    
    
    // 1: RANDOM COORDINATES
    func randomCoordinate(max: CGFloat) -> CGFloat {
        return CGFloat.random(in: 0...max)
    }
    // 2: RANDOM SIZE
    func randomSize() -> CGFloat {
        return CGFloat(Int.random(in: 10...100))
    }
    
   /* // 3. RANDOM SCALE
    func randomScale() -> CGFloat {
        return CGFloat(Double.random(in: 0.1...1.0))
    }*/
    
    // 4: RANDOM SPEED
    func randomSpeed() -> Double {
        return Double.random(in: 0.025...0.5)
    }
    // 5: RANDOM DELAY
    func randomDelay() -> Double {
        return Double.random(in: 0...2)
    }
    
    // MARK: - BODY
    var body: some View {
        
     GeometryReader { geometry in
            ZStack {
                ForEach(0...randomSnowflakes, id: \.self) { item in
                    SnowflakeView(id: 1001, date: Date())
                        .opacity(0.75)
                        .frame(width: randomSize(), height: randomSize(), alignment: .center)
                       // .scaleEffect(isAnimating ? randomScale() : 1)
                        .offset(y: CGFloat(positionY))
                        .position(
                          x: randomCoordinate(max: geometry.size.width),
                          y: randomCoordinate(max: geometry.size.height)
                        )
                        .animation(Animation.linear(duration: 5.0)
                                .repeatForever()
                                .speed(randomSpeed())
                                .delay(randomDelay())
                 )
                    
                    .onAppear(perform: {
                        isAnimating = true
                        positionY += 150                    })
                } //: LOOP
                //.background(Color.black)
                
            } // ZSTACK
            .background(Color.black)
            .drawingGroup()
            //.ignoresSafeArea()
        } //: GEOMETRY
        
    }
}

// MARK: - PREVIEW
*/
