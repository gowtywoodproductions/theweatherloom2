//
//  DigitalSnowCanvas.swift
//  The WeatherLoom
//
//  Created by Owner on 2/11/21.
//

import SwiftUI

struct DigitalSnowCanvas: View {
       @StateObject var core = ModelData(snowflakeCount: 0)
       let date: Date

       var body: some View {
           if #available(iOS 15.0, *) {
               Canvas(renderer: renderer) {
                   // Column views tagged as Canvas symbols
                   ForEach(core.snowflakes) { snowflake in
                       SnowflakeView(id: snowflake.id, date: date)
                           .tag(snowflake.id)
                   }
               }
               .onChange(of: date) { (date: Date) in
                   // Add columns progressively, every `addColumnRate` seconds, up to a maximum of `maxColumns`
                   if core.snowflakes.count < ModelData.maxSnowflakes
                        && Date().timeIntervalSinceReferenceDate > core.lastAddDate.addingTimeInterval(ModelData.addSnowflakeRate).timeIntervalSinceReferenceDate {
                       
                       core.addSnowflake()
                   }
                   
                   // update columns data (e.g., position, depth, character count, etc)
                   for idx in 0..<core.snowflakes.count {
                       core.snowflakes[idx].fallDown(date: date)
                   }
               }
               .environmentObject(core)
           } else {
               // Fallback on earlier versions
           }
       }
       
    @available(iOS 15.0, *)
    func renderer(context: inout GraphicsContext, size: CGSize) {
           
           // Draw every column
           for snowflake in core.snowflakes.sorted(by: { $0.z > $1.z }) {
               context.drawLayer { context in
                   
                   if let resolved = context.resolveSymbol(id: snowflakes.id) {
                       
                       // Column location
                       let pt = CGPoint(x: snowflakes.origin.x * size.width,
                                        y: snowflakes.origin.y * size.height + snowflakes.offset)
                       
                       // Blur and scale effect, based on column's depth
                      // context.addFilter(.blur(radius: column.z * 3))
                      // context.scaleBy(x: 1 - column.z * 0.3, y: 1 - column.z * 0.3)
                       
                       // Draw column
                       context.draw(resolved, at: pt, anchor: .top)
                   }
               }
           }
       }
   }

